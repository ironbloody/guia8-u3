#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;
#include "Ordenamiento.h"

#define TRUE 0
#define FALSE 1

// Funcion para imprimir vector.
void Ordenamiento::imprimir_vector(int a[], int n) {
  // For para imprimir
  for (int i=0; i<n; i++) {
    cout << "a[" << i << "]=" << a[i] << " ";
  }
  cout << endl;
  cout << endl;
}

// Funcion para metodo quicksort
void Ordenamiento::ordena_quicksort(int a[], int n) {
  
  // Creacion de variables.
  int tope, ini, fin, pos;
  int pilamenor[100];
  int pilamayor[100];
  int izq, der, aux, band;
  
  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = n-1;
  
  // Mientras el tope sea mayor o igual a 0.
  while (tope >= 0) {
    // Se define inicio y fin.
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    // Se le resta uno a tope.
    tope = tope - 1;
    
    // reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = TRUE;
    
    // Mientras band sea igual a TRUE.
    while (band == TRUE) {
      // Mientras se cumpla esta condicion.
      while ((a[pos] <= a[der]) && (pos != der))
        // Se le resta uno a der.
        der = der - 1;
      
      // Si la pos es der.
      if (pos == der) {
        // El band sera FALSE.
        band = FALSE;
      //Sino.
      } else {
        // Se crea aux para cambiar posiciones
        aux = a[pos];
        a[pos] = a[der];
        a[der] = aux;
        pos = der;
        
        // Mientras se cumpla esta condicion.
        while ((a[pos] >= a[izq]) && (pos != izq))
          // Se le suma uno a izq.
          izq = izq + 1;
        
        // Si la pos es igual a izq.
        if (pos == izq) {
          // band se iguala a FALSE.
          band = FALSE;
        // Sino.
        } else {
          // Se cambian las posiciones.
          aux = a[pos];
          a[pos] = a[izq];
          a[izq] = aux;
          pos = izq;
        }
      }
    }
    // Si inicio es menor que posicion menos uno.
    if (ini < (pos - 1)) {
      // Se la suma uno a tope.
      tope = tope + 1;
      // Pilamenor se iguala ini .
      pilamenor[tope] = ini;
      // Pilamayor se giauala a pos menos 1.
      pilamayor[tope] = pos - 1;
    }
    
    // Si inicio es mayor que posicion mas uno.
    if (fin > (pos + 1)) {
      // Se la suma uno a tope.
      tope = tope + 1;
      // Pilamenor se iguala a pos mas uno.
      pilamenor[tope] = pos + 1;
      // Pilamayor se iguala fin.
      pilamayor[tope] = fin;
    }
  }
}

// Funcion para metodo seleccion.
void Ordenamiento::ordena_seleccion(int a[], int n) {
  // Creacion de variables.
  int i, menor, k, j;
  
  // For para recorrer la lista.
  for (i=0; i<=n-2; i++) {
    // Menor se iguala a la primera posicion.
    menor = a[i];
    // Se guarda la posicion.
    k = i;
    
    // For para recorrer.
    for (j=i+1; j<=n-1; j++) {
      // Si la segunda posicion es menor que el menor.
      if (a[j] < menor) {
        // Pasa pasa a la siguiente posicion.
        menor = a[j];
        // Se guarda la posicion.
        k = j;
      }
    }
    // Se guardan los datos.
    a[k] = a[i];
    a[i] = menor;
  }
}