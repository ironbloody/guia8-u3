**Guia 8 Unidad 3**                                                                                            
Programa basado en metodos de ordenamieto                                                                          

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                      
Ubuntu                                                                                                                                                                                                                                                                                                                                                               
**Instalación**                                                                                                                                       
-Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make.


**Problematica**                                                                                                                                       
Reescribir un programa que esta en c a c++ y modificarlo para que logre ordenar un mismo conjunto de elementos utilizando los metodos seleccion y quicksort.                           

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.                    
AL abrir el programa le pedira que ingrese el numero de elementos que tendra el vector, luego de eso le preguntara si desea ver el contenido de los vectores. Si pulsa s el programa le mostrara el contenido de estos y los milisegundos que se demoro el metodo quicksort y seleccion en ordenar dichos elementos. Si pulsa n el programa solo le mostrara la cantidad de tiempo que se demoraron los metodos en ordenar el vector.

                                                                                            
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:
- Stdio
- Time
- Stdlib
- String
- Iostream     
- Chrono
- Ctime
- Ratio

**Versionado**                                                                                                                                        
Version 1.2                                                                                                                                       

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               # Guía8-U3

