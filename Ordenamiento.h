#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H


class Ordenamiento {
  private:
    
  public:
    // Imprime el vector.
    void imprimir_vector(int a[], int n);
    // Metodo quicksort.
    void ordena_quicksort(int a[], int n);
    // Metodo seleccion.
    void ordena_seleccion(int a[], int n);
};
#endif