#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <ratio>
using namespace std;
#include "Ordenamiento.h"
using namespace std::chrono;


#define LIMITE_SUPERIOR 1000000
#define LIMITE_INFERIOR 1

// Funcion principal.
int main() {
  
  // Creacion de variables.
  string opcion;
  int n;
  // Se pide que se ingrese el tamaño del vector.
  cout << "Ingrese el numero: " << endl;
  // Se registra el vector.
  cin >> n;
  // Condicion para que se puedan ingresar numeros mayores a 1 millon o menores a 1.
  while (n > 1000000 || n < 1 ){
    cout << "Escriba el numero de nuevo: " << endl;
    // Se registra el vector.
    cin >> n;
  }
  // Creacion de variables.
  int a[n];
  int b[n];
  int i;
  int elemento;
  // Llamado de la clase.
  Ordenamiento o = Ordenamiento();

  // llena el vector.
  srand (time(NULL));
  for (i=0; i<n; i++) {
    elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
    a[i] = elemento;
    b[i] = elemento;
  }
  
  // Creacion de variables de tiempo.
  // llama al método de ordenamiento seleccion.
  high_resolution_clock::time_point t1 = high_resolution_clock::now();
  o.ordena_seleccion(b,n);
  high_resolution_clock::time_point t2 = high_resolution_clock::now();
  
  // llama al método de ordenamiento quicksort.
  high_resolution_clock::time_point t3 = high_resolution_clock::now();
  o.ordena_quicksort(a, n);
  high_resolution_clock::time_point t4 = high_resolution_clock::now();
  // Metodo para calcular los segundos.
  duration<double> time_span = duration_cast<duration<double>>(t2-t1);
  duration<double> time_span2 = duration_cast<duration<double>>(t4-t3);
  // Opcion para que el usuario decida si ver el contenido de los vectores.
  cout << "¿Desea ver el contenido de los vectores? s/n" << endl;
  // Se registra la opocion.
  cin >> opcion;
  // Si la opcion es s.
  if(opcion == "s"){
    o.imprimir_vector(a, n);
    cout << "-----------------------------------------" << endl;
    cout << "Metodo              |Tiempo              " << endl;
    cout << "-----------------------------------------" << endl;
    std::cout << "Seleccion           |" << time_span.count() << " milisegundos\n" << std::endl;
    std::cout << "Quicksort           |" << time_span2.count()  << " milisegundos\n" << std::endl;
    cout << "-----------------------------------------" << endl;
    cout << "Seleccion: "; 
    o.imprimir_vector(b, n);
    cout << "Quicksort: "; 
    o.imprimir_vector(a,n);
  }
  // Sino.
  else{
    cout << "-----------------------------------------" << endl;
    cout << "Metodo              |Tiempo              " << endl;
    std::cout << "Seleccion           |" << time_span.count() << " milisegundos\n" << std::endl;
    std::cout << "Quicksort           |" << time_span2.count()  << " milisegundos\n" << std::endl;
    cout << "-----------------------------------------" << endl;
  }
  
  
  return 0;
}